<?php

/**
 * XHProfRuns_Default is the default implementation of the
 * iXHProfRuns interface for saving/fetching XHProf runs.
 *
 */
class XHProfUIRuns_Default extends XHProfRuns_Default implements iXHProfUIRuns {
  public function getDir(){
    return $this->dir;
  }

  public function getRuns($stats, $limit = 50, $skip = 0) {
    //$files = $this->scanXHProfDir($this->dir, variable_get('site_name', ''));
    $files = $this->scanXHProfDir('/tmp', variable_get('site_name', ''));
    $files = array_map(function($f) {
      $f['date'] = strtotime($f['date']);
      return $f;
    }, $files);
    dsm($files);
    return $files;
  }

  public function getCount(){}
  public function getHardHit($criteria){}
  public function getDistinct($data){}
  public static function getNextAssoc($resultSet){}
  public function getRunComparativeData($url, $c_url){}

  public function scanXHProfDir($dir, $source = NULL) {
    if (is_dir($dir)) {
      $runs = array();
      foreach (glob("$dir/*.$source.*") as $file) {
        list($run, $source) = explode('.', basename($file));
        $runs[] = array(
          'run_id' => $run,
          'source' => $source,
          'basename' => htmlentities(basename($file)),
          'date' => date("Y-m-d H:i:s", filemtime($file)),
        );
      }
    }
    return array_reverse($runs);
  }
}
