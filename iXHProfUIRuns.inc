<?php

interface iXHProfUIRuns extends iXHProfRuns {
  /**
  * This function gets runs based on passed parameters, column data as key, value as the value. Values
  * are escaped automatically. You may also pass limit, order by, group by, or "where" to add those values,
  * all of which are used as is, no escaping.
  *
  * @param array $stats Criteria by which to select columns
  * @return resource
  */
  public function getRuns($stats, $limit = 50, $skip = 0);

  public function getCount();


  /**
   * Obtains the pages that have been the hardest hit over the past N days, utalizing the getRuns() method.
   *
   * @param array $criteria An associative array containing, at minimum, type, days, and limit
   * @return resource The result set reprsenting the results of the query
   */
  public function getHardHit($criteria);

  public function getDistinct($data);

  public static function getNextAssoc($resultSet);

  /**
   * Get comparative information for a given URL and c_url, this information will be used to display stats like how many calls a URL has,
   * average, min, max execution time, etc. This information is pushed into the global namespace, which is horribly hacky.
   *
   * @param string $url
   * @param string $c_url
   * @return array
   */
  public function getRunComparativeData($url, $c_url);
}

