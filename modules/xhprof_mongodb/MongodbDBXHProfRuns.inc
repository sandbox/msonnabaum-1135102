<?php

/**
 * XHProfRuns_Default is the default implementation of the
 * iXHProfRuns interface for saving/fetching XHProf runs.
 *
 * It stores/retrieves runs to/from a filesystem directory
 * specified by the "xhprof.output_dir" ini parameter.
 *
 * @author Kannan
 */
class MongodbXHProfRuns implements iXHProfUIRuns {

  private $dir = '';

  private function gen_run_id($type) {
    return uniqid();
  }

  private function file_name($run_id, $type) {
    $file = "$run_id.$type";

    if (!empty($this->dir)) {
      $file = $this->dir . "/" . $file;
    }
    return $file;
  }

  public function __construct($dir = null) {
  }

  /**
  * This function gets runs based on passed parameters, column data as key, value as the value. Values
  * are escaped automatically. You may also pass limit, order by, group by, or "where" to add those values,
  * all of which are used as is, no escaping.
  *
  * @param array $stats Criteria by which to select columns
  * @return resource
  */
  public function getRuns($stats, $limit = 50, $skip = 0) {
    $collection = mongodb_collection('xhprof');
    $sort = isset($_GET['order']) ? $_GET['order'] : 'date';
    $sort_direction_str = isset($_GET['sort']) ? $_GET['sort'] : 'desc';
    $sort_direction = array(
      'asc' => 1,
      'desc' => -1,
    );

    $cursor = $collection
    ->find(array())
    ->limit($limit)
    ->skip($skip)
    ->sort(array(strtolower($sort) => $sort_direction[$sort_direction_str]));
    ;
    $runs = array();
    foreach ($cursor as $id => $run) {
      $run['run_id'] = $id;
      $runs[] = $run;
    }

    $file_name = $this->file_name($run_id, $type);
    $run_desc = "XHProf Run (Namespace=$type)";

    return $runs;
  }

  public function getCount() {
    $collection = mongodb_collection('xhprof');
    $count = $collection->count();
    return $count;
  }


  /**
   * Obtains the pages that have been the hardest hit over the past N days, utalizing the getRuns() method.
   *
   * @param array $criteria An associative array containing, at minimum, type, days, and limit
   * @return resource The result set reprsenting the results of the query
   */
  public function getHardHit($criteria) {
    //call thing to get runs
    $criteria['select'] = "distinct(`{$criteria['type']}`), count(`{$criteria['type']}`) AS `count` , sum(`wt`) as total_wall, avg(`wt`) as avg_wall";
    unset($criteria['type']);
    $criteria['where'] = "DATE_SUB(CURDATE(), INTERVAL {$criteria['days']} DAY) <= `timestamp`";
    unset($criteria['days']);
    $criteria['group by'] = "url";
    $criteria['order by'] = "count";
    $resultSet = $this->getRuns($criteria);

    return $resultSet;
  }

  public function getDistinct($data) {
    $sql['column'] = $data['column'];
    $query = "SELECT DISTINCT(`{$sql['column']}`) FROM `details`";
    $rs = db_query($query);
    return $rs;
  }

  public static function getNextAssoc($resultSet) {
    return mysqli_fetch_assoc($resultSet);
  }


  public function get_run($run_id, $type, &$run_desc) {
    $collection = mongodb_collection('xhprof');
    $run_desc = "XHProf Run (Namespace=$type)";

    $run = $collection->findOne(array('_id' => (string)$run_id));
    return $run['run_data'];
  }

  public function save_run($xhprof_data, $type, $run_id = null) {
    if ($run_id === null) {
      $run_id = $this->gen_run_id($type);
    }

    $mongo_id = $run_id;

    module_load_include('module', 'mongodb');
    $collection = mongodb_collection('xhprof');

    $entry = array();
    $entry['_id'] = (string)$mongo_id;
    $entry['run_data'] = $xhprof_data;
    $entry['get'] = serialize($_GET);
    $entry['cookie'] = serialize($_COOKIE);
    $entry['date'] = $_SERVER['REQUEST_TIME'];
    $entry['pmu'] = isset($xhprof_data['main()']['pmu']) ? $xhprof_data['main()']['pmu'] : '';
    $entry['wt']  = isset($xhprof_data['main()']['wt'])  ? $xhprof_data['main()']['wt']  : '';
    $entry['cpu'] = isset($xhprof_data['main()']['cpu']) ? $xhprof_data['main()']['cpu'] : '';



    $entry['path'] = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : $_SERVER['PHP_SELF'];
    $entry['servername'] = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : '';

    $collection->save($entry);

    return $run_id;
  }

  /**
   * Get comparative information for a given URL and c_url, this information will be used to display stats like how many calls a URL has,
   * average, min, max execution time, etc. This information is pushed into the global namespace, which is horribly hacky.
   *
   * @param string $url
   * @param string $c_url
   * @return array
   */
  public function getRunComparativeData($url, $c_url) {
    $url = $url;
    $c_url = $c_url;
    //Runs same URL
    //  count, avg/min/max for wt, cpu, pmu
    $query = "SELECT count(`id`), avg(`wt`), min(`wt`), max(`wt`),  avg(`cpu`), min(`cpu`), max(`cpu`), avg(`pmu`), min(`pmu`), max(`pmu`) FROM `details` WHERE `url` = '$url'";
    $rs = db_query($query);
    $row = mysqli_fetch_assoc($rs);
    $row['url'] = $url;

    $row['95(`wt`)'] = $this->calculatePercentile(array('count' => $row['count(`id`)'], 'column' => 'wt', 'type' => 'url', 'url' => $url));
    $row['95(`cpu`)'] = $this->calculatePercentile(array('count' => $row['count(`id`)'], 'column' => 'cpu', 'type' => 'url', 'url' => $url));
    $row['95(`pmu`)'] = $this->calculatePercentile(array('count' => $row['count(`id`)'], 'column' => 'pmu', 'type' => 'url', 'url' => $url));

    global $comparative;
    $comparative['url'] = $row;
    unset($row);

    //Runs same c_url
    //  count, avg/min/max for wt, cpu, pmu
    $query = "SELECT count(`id`), avg(`wt`), min(`wt`), max(`wt`),  avg(`cpu`), min(`cpu`), max(`cpu`), avg(`pmu`), min(`pmu`), max(`pmu`) FROM `details` WHERE `c_url` = '$c_url'";
    $rs = db_query($query);
    $row = mysqli_fetch_assoc($rs);
    $row['url'] = $c_url;
    $row['95(`wt`)'] = $this->calculatePercentile(array('count' => $row['count(`id`)'], 'column' => 'wt', 'type' => 'c_url', 'url' => $c_url));
    $row['95(`cpu`)'] = $this->calculatePercentile(array('count' => $row['count(`id`)'], 'column' => 'cpu', 'type' => 'c_url', 'url' => $c_url));
    $row['95(`pmu`)'] = $this->calculatePercentile(array('count' => $row['count(`id`)'], 'column' => 'pmu', 'type' => 'c_url', 'url' => $c_url));

    $comparative['c_url'] = $row;
    unset($row);
    return $comparative;
  }
}
